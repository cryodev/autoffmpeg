﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace AUTOffmpeg
{
    public partial class MainWindow : Form
    {
        //Global variable that stores the directories dragged in the window
        string[] directories;

        public String settingsPath;
        public String filepath;
        public bool hasXML;

        XMLParser settings = new XMLParser();

        //Window constructor, instantiate necessary events
        public MainWindow()
        {
            InitializeComponent();
            this.textBox1.AllowDrop = true;
            this.textBox1.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.textBox1.DragDrop += new DragEventHandler(Form1_DragDrop);
            this.convertButton.MouseDown += new MouseEventHandler(ConvertPaths);

            this.fpsbox.SelectedIndex = 0;
            //settings XML parser intialization
            //if an XML exists, populate the Checklist and set hasXML to true

            if (settings.InitializeXML())
            {
                hasXML = true;
                foreach (Preset preset in settings.presets)
                {
                    this.presetBox.Items.Add(preset.name, false);
                }
            }
            else
            {
                this.noxml.Visible = true;
            }
        }


        //Dragging a directory onto the window event - copy 
        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        //Drag and Drop event - if directories exist, clear and get new directories
        //Else just get new directories
        void Form1_DragDrop(object sender, DragEventArgs e)
        {

            if (directories != null)
            {
                Array.Clear(directories, 0, directories.Length);
            }
            directories = (string[])e.Data.GetData(DataFormats.FileDrop);
            this.textBox1.Clear();
            foreach (string item in directories)
            {
                String appendLine = String.Format(item, Environment.NewLine);
                this.textBox1.AppendText(appendLine);
                this.textBox1.AppendText(Environment.NewLine);
            }

        }

        //Mouseclick event for the Convert button - begin parsing directory contents if clicked
        void ConvertPaths(object sender, MouseEventArgs e)
        {

            //Only do anything if directories is not null
            if (directories != null)
            {
                //For each directory, use ffmpeg to convert the directory's contents
                foreach (string directory in directories)
                {

                    //Create individual parser per directory
                    PathHandler parser = new PathHandler(directory);

                    //Check for Final folders and get files from latest if it exists -> targetFiles
                    if (parser.CheckRootFinal(directory))
                    {
                        Console.WriteLine("Final folder exists");
                    }
                    //If there is no final folder, get the .tga files inside the directory
                    else
                    {
                        parser.CheckRootTGA(directory);
                        Console.WriteLine("Final folder does not exist");
                    }

                    //Ensure that there are TargetFiles resulting from the parse before continuing
                    if (parser.GetTargetFiles() != null)
                    {

                        //Get the required tokens for ffmpeg command line
                        IEnumerable<string> filetokens = parser.GetTargetFiles();
                        string filetoken = filetokens.First().Substring(0, filetokens.First().Length - 8);
                        Console.WriteLine(filetoken);
                        string filename = Path.GetFileName(directory);
                        Console.WriteLine(filename);
                        Console.WriteLine(directory);
                        string destination = directory.Substring(0, directory.LastIndexOf('\\'));

                        //Check to see if non-float value in FPS box
                        try
                        {
                            float.Parse(this.fpsbox.Text.ToString());
                        }
                        catch
                        {
                            Console.WriteLine("Caught invalid cast");
                            this.errorLabel.Visible = true;
                            break;
                        }

                        //If ok then turn off error label and parse fpsbox text
                        this.errorLabel.Visible = false;
                        string fps = this.fpsbox.Text.ToString();
                        Console.WriteLine(fps);

                        if (hasXML)
                        {
                            int[] selected = presetBox.CheckedIndices.Cast<int>().ToArray();

                            //For every selected preset, encode one by one with the preset's arguments.
                            foreach (int i in selected)
                            {
                                string name = settings.presets.ElementAt(i).name;
                                string arguments = settings.presets.ElementAt(i).arguments;
                                string type = settings.presets.ElementAt(i).type;

                                string command = "/C \"L:\\Software\\FFmpeg\\ffmpeg-20141116\\bin\\ffmpeg.exe\" -r " + fps + " -i " + filetoken +
                                "%04d.tga " + arguments + " -r " + fps + " " + destination + "\\" + filename + "_" + name + "." + type;
                                //Pass the resulting command to ffmpeg
                                Process cmd = Process.Start("CMD.exe", command);
                                cmd.WaitForExit();
                            }
                        }
                        else
                        {
                            string command = "/C \"L:\\Software\\FFmpeg\\ffmpeg-20141116\\bin\\ffmpeg.exe\" -r " + fps + " -i " + filetoken +
                                "%04d.tga -vcodec libx264 -preset slower -profile:v high -level 4.2 -pix_fmt yuv420p -b:v 24000k -minrate 16000k " +
                                "-maxrate 32000k -bufsize 24000k -r " + fps + " " + destination + "\\" + filename + ".mp4";
                            //Pass the resulting command to ffmpeg
                            Process cmd = Process.Start("CMD.exe", command);
                            cmd.WaitForExit();
                        }


                    }
                }
            }
        }
    }
}
