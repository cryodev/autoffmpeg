﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace AUTOffmpeg
{
    class XMLParser
    {
        //XMLDocument for using xml parsing libraries
        XmlDocument settingsxml = new XmlDocument();

        //Get each Preset node of the XML
        //and parse each node, passing to a list of type Preset
        public XmlNodeList allpreset;
        public List<Preset> presets = new List<Preset>();

        public XMLParser()
        {


        }

        public bool InitializeXML()
        {
            string localdirectory = AppDomain.CurrentDomain.BaseDirectory;

            //Attempt to parse the xml
            try
            {
                settingsxml.Load(localdirectory + "settings.xml");
            }
            catch
            {
                Console.WriteLine("Not a valid XML");
                return false;
            }

            //Get every Preset node in Presets in the XML
            allpreset = settingsxml.DocumentElement.SelectNodes("/Presets/Preset");

            foreach (XmlNode preset in allpreset)
            {
                //Instantiate a new Preset for this preset
                Preset thispreset = new Preset();

                thispreset.name = preset.SelectSingleNode("Name").InnerText;
                thispreset.arguments = preset.SelectSingleNode("Arguments").InnerText;
                thispreset.type = preset.SelectSingleNode("Type").InnerText;
                thispreset.id = preset.Attributes["ID"].Value;

                presets.Add(thispreset);
            }
            return true;
        }
    }
}
