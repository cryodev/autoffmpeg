﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AUTOffmpeg
{
    //The Preset type contains the XML contents to make accessing things easier
    class Preset
    {
        public string name;
        public string arguments;
        public string id;
        public string type;
    }
}
