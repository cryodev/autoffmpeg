﻿namespace AUTOffmpeg
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.convertButton = new System.Windows.Forms.Button();
            this.fpsbox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.errorLabel = new System.Windows.Forms.Label();
            this.presetBox = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.noxml = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(16, 52);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(402, 484);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(317, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "Drag directory folders containing TGA \r\nsequences you wish to convert into this b" +
    "ox.";
            // 
            // convertButton
            // 
            this.convertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertButton.Location = new System.Drawing.Point(614, 127);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(121, 42);
            this.convertButton.TabIndex = 2;
            this.convertButton.Text = "ENCODE";
            this.convertButton.UseVisualStyleBackColor = true;
            // 
            // fpsbox
            // 
            this.fpsbox.AllowDrop = true;
            this.fpsbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.fpsbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fpsbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fpsbox.FormattingEnabled = true;
            this.fpsbox.Items.AddRange(new object[] {
            "23.976",
            "24",
            "29.97",
            "30"});
            this.fpsbox.Location = new System.Drawing.Point(614, 52);
            this.fpsbox.Name = "fpsbox";
            this.fpsbox.Size = new System.Drawing.Size(121, 21);
            this.fpsbox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(614, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Target FPS:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(611, 85);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(113, 26);
            this.errorLabel.TabIndex = 5;
            this.errorLabel.Text = "Invalid FPS: \r\nPlease enter a number";
            this.errorLabel.Visible = false;
            // 
            // presetBox
            // 
            this.presetBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.presetBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.presetBox.CheckOnClick = true;
            this.presetBox.FormattingEnabled = true;
            this.presetBox.Location = new System.Drawing.Point(435, 52);
            this.presetBox.Name = "presetBox";
            this.presetBox.Size = new System.Drawing.Size(158, 482);
            this.presetBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(431, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Select presets:";
            // 
            // noxml
            // 
            this.noxml.AutoSize = true;
            this.noxml.BackColor = System.Drawing.Color.WhiteSmoke;
            this.noxml.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noxml.ForeColor = System.Drawing.Color.Red;
            this.noxml.Location = new System.Drawing.Point(453, 61);
            this.noxml.Name = "noxml";
            this.noxml.Size = new System.Drawing.Size(120, 60);
            this.noxml.TabIndex = 8;
            this.noxml.Text = "No settings.xml!\r\nWill use default\r\nsettings.";
            this.noxml.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.noxml.Visible = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 545);
            this.Controls.Add(this.noxml);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.presetBox);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fpsbox);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Name = "MainWindow";
            this.Text = "AUTOffmpeg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.ComboBox fpsbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.CheckedListBox presetBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label noxml;


    }
}

