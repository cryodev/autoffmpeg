﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AUTOffmpeg
{
    class PathHandler
    {
        private String root;
        List<string> imageList = new List<string>();

        //targetFiles is global variable that contains current target list for processing
        IEnumerable<string> targetFiles;

        //Constructor
        public PathHandler(String directory)
        {
            root = directory;
        }

        //CheckRootFinal checks specified or root directory to see if any final folders exist
        public bool CheckRootFinal(string checkDir)
        {
            int indexValue;
            int highestValue = 0;
            String highestValueStr = "";

            bool contains;
            IEnumerable<string> finalDirs = Directory.EnumerateDirectories(checkDir, "Final*");
            contains = (finalDirs.Any());

            //If any exist, get the highest Final folder
            Console.WriteLine(contains);
            if (contains)
            {
                foreach (string finalDir in finalDirs)
                {
                    string thisDir = Path.GetFileName(finalDir);
                    indexValue = Convert.ToInt32(thisDir.Substring(thisDir.Length - 2));

                    if (indexValue > highestValue)
                    {
                        highestValue = indexValue;
                        highestValueStr = thisDir.Substring(thisDir.Length - 2);
                    }
                    Console.WriteLine(highestValue);
                }

                highestValueStr = "*" + highestValueStr;
                finalDirs = Directory.GetDirectories(checkDir, highestValueStr);

                foreach (string highestDir in finalDirs)
                {
                    targetFiles = Directory.EnumerateFiles(highestDir, "*.tga");
                }
            }
            return contains;
        }

        //CheckRootTGA checks specified or root directory to see if any TGA files exist
        public bool CheckRootTGA(string checkDir)
        {
            Console.WriteLine(checkDir);
            targetFiles = Directory.EnumerateFiles(checkDir, "*.tga");

            bool contains;
            contains = (targetFiles == null || !targetFiles.Any());

            Console.WriteLine(contains);
            return contains;
        }
        public bool CheckRootTGA()
        {
            Console.WriteLine(root);
            targetFiles = Directory.EnumerateFiles(root, "*.tga");

            bool contains;
            contains = (targetFiles == null || !targetFiles.Any());

            Console.WriteLine(contains);
            return contains;
        }

        //Getter, return target .tga files
        public IEnumerable<string> GetTargetFiles()
        {
            return targetFiles;
        }

    }
}
